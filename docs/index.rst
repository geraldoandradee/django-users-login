.. Django Users Manager documentation master file, created by
   sphinx-quickstart on Sat Jun 29 18:19:46 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
Django Users Login
==================

Django Users Login is a extension interface to users in django application login. It's inspired in devise (Ruby gem for
RoR) but not so complex like it.

The main task is authenticate user using django authentication:

* Authenticate users it's a repetitive task to implement this although django make this easier for us;
* Provide graphic default template to do it.

Links
-----

* `Documentation <https://django-users-login.readthedocs.org/en/latest/>`_
* `Repository <http://bitbucket.org/quein/django-users-login/>`_

Revisions
---------

0.1
```

* Template to login, logout, password recover;
* Django>=1.5.4 support;
* Customizable template.

Usage
=====



.. toctree::
   :maxdepth: 4

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

