from django.conf.urls import patterns, include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django_users_login import urls
admin.autodiscover()

urlpatterns = patterns('',

    # Examples:
    # url(r'^$', 'aqui.views.home', name='home'),
    # url(r'^aqui/', include('aqui.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', include(urls)),

    url(r'^$', 'core.views.home', name='home'),
    url(r'^must-be-logged-to-see$', 'core.views.logged_in', name='logged_in'),
)
