# Create your views here.
from __future__ import unicode_literals
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required


def home(request):
    return render_to_response('home.html', {'request': request})


@login_required
def logged_in(request):
    return render_to_response('logged_in.html')


def logged_out(request):
    if request.user.is_authenticated():
        pass  # Do something for authenticated users.
    else:
        pass  # Do something for anonymous users.
    return render_to_response('logged_out.html')