#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)
import logging
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseForbidden, HttpResponseRedirect, HttpResponse, HttpResponseNotFound
# from django.utils.translation import ugettext as _
from django.views.generic import View
# from django.contrib.auth.models import User
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.views import password_reset, password_change, password_change_done, password_reset_complete, \
    password_reset_done
from .forms import FormLogin
from django.contrib.auth.forms import PasswordResetForm, AuthenticationForm
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
log = logging.getLogger('django')


class Login(View):
    def post(self, request):
        """
        Login: post
        -----------

        Used to try to login the user with given credentials.

        :param request:
        :return: HttpResponse
        """
        form = FormLogin(request.POST)
        if form.is_valid():
            # let's test if user can authenticate, user must be registered in users data table
            usuario = authenticate(username=form.data['username'], password=form.data['password'])
            if usuario is not None and usuario.is_active:
                login(request=request, user=usuario)
                try:
                    next_url = request.POST['next']
                except MultiValueDictKeyError, e:
                    log.info("Redirecting to defautl url in [%s]" % settings.LOGIN_REDIRECT_URL)
                    next_url = settings.LOGIN_REDIRECT_URL
                return HttpResponseRedirect(next_url)
            else:
                form.errors['Autenticação'] = 'Usuário e/ou senha incorretos'

        return render(request, 'django_users_login/login.html', {'form': form})

    def get(self, request):
        form = AuthenticationForm()
        if request.user.is_authenticated():
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
        try:
            next_url = request.GET['next']
        except Exception, e:
            next_url = settings.LOGIN_REDIRECT_URL

        return render(request, 'django_users_login/login.html', {'form': form, 'next_url': next_url})


class Logout(View):
    """
    Logout
    ======

    Logout view to get it out with the user.

    """

    def get(self, request):
        logout(request=request)
        return redirect(to=settings.LOGIN_REDIRECT_URL)


class ForgotPassword(View):
    # TODO: stopped here.
    """
    class PasswordChangeForm
    A form for allowing a user to change their password.

    class PasswordResetForm
    A form for generating and emailing a one-time use link to reset a user’s password.

    class SetPasswordForm
    A form that lets a user change his/her password without entering the old password.

    class UserChangeForm
    A form used in the admin interface to change a user’s information and permissions.

    class UserCreationForm
    A form for creating a new user.
    """

    def get(self, request):
        form = PasswordResetForm()
        return render(request, 'django_users_login/forgot_password.html', {'form': form})

    def post(self, request):

        try:
            email = request.POST['email']
        except MultiValueDictKeyError, e:
            log.critical('There\'s no email in POST request: [%s]' % e)
            return HttpResponseNotFound('ERROR: e-mail not provided.')

        user = User.objects.get(email=email)

        if user:
            is_admin_site = user.is_superuser
            template_name = 'django_users_login/password_reset_form.html'
            email_template_name = 'django_users_login/password_reset_email.html'
            subject_template_name = 'registration/password_reset_subject.txt'
            password_reset_form = PasswordResetForm
            token_generator = default_token_generator
            post_reset_redirect = None
            from_email = None

            if request.method == "POST":
                form = password_reset_form(request.POST)
                if form.is_valid():
                    opts = {
                        'use_https': request.is_secure(),
                        'token_generator': token_generator,
                        'from_email': from_email,
                        'email_template_name': email_template_name,
                        'subject_template_name': subject_template_name,
                        'request': request,
                    }
                    if is_admin_site:
                        opts = dict(opts, domain_override=request.get_host())
                    form.save(**opts)
                    return HttpResponseRedirect(settings.LOGOUT_URL)

        else:
            log.critical('[SECURITY ERROR]: User does not exists: [%s]' % email)
            return HttpResponseNotFound('Page does not exists')


        # return password_reset(request, 'django_users_login/password_reset_email.html')
        #email = request.POST['email']
        #form = PasswordResetForm({'email': email})
        #return form.save(email_template_name='django_users_login/password_reset_email.html', use_https=False,
        #                 from_email=None, request=request)


class ChangePassword(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        super(ChangePassword, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        return render_to_response('django_users_login/password_change_form.html',
                                  context_instance=RequestContext(request))

    def post(self, request):
        """
        post
        ----

        This will change the password.

        """
        old_password = request.POST['old_password']
        new_password = request.POST['new_password']
        confirm_new_password = request.POST['confirm_new_password']

        return render_to_response('django_users_login/password_change_done.html')


class PasswordResetDone(View):
    def get(self, request):
        return password_reset_done(request, template_name='django_users_login/password_reset_done.html')

#def my_password_reset(request, template_name='path/to/my/template'):
#    return password_reset(request, template_name)
#def reset_password(email, from_email, template='registration/password_reset_email.html'):
#   """
#   Reset the password for all (active) users with given E-Mail address
#   """
#   form = PasswordResetForm({'email': email})
#   return form.save(from_email=from_email, email_template_name=template)

class Profile(View):
    """
    Profile
    =======

    View for profile data

    """

    def post(self, request):
        return render(request, 'core/index.html')

    def get(self, request):
        return render(request, 'core/index.html')


class SignUp(View):
    def post(self, request):
        form = FormLogin(request.POST)
        if form.is_valid():
            if request.is_ajax():
                pass
            else:
                pass

        return render(request, 'signup.html')

    def get(self, request):
        return HttpResponse('Not implemented yet.')
        # if SIGN_UP_MODE:
        #     return render(request, 'signup.html')
        # else:
        #     return HttpResponseForbidden(_('operation not permitted'))


        # class Manage(View):
        #     """
        #     Manage
        #     ======
        #
        #     This class has all methods to manage users. You can edit, delete, create and update all users.
        #
        #     """
        #
        #     task = None
        #
        #     def suspend_user(self, request, id):
        #         pass
        #
        #     def reset_password_user(self, request, id):
        #         pass
        #
        #     def edit_user(self, request, id):
        #         pass
        #
        #     def delete_user(self, request, id):
        #         pass
        #
        #     def create_user(self, request, id):
        #         pass
        #
        #     def post(self, request):
        #         self.get(request)
        #
        #     def put(self, request):
        #         pass
        #
        #     def get(self, request, method, id=None):
        #         try:
        #             getattr(self, self.task)(request, id) # this mapping all requests to this class
        #         except Exception as e:
        #             pass
        #
        #
        # # class ListUsers(View):
        # #     """
        # #     ListUsers
        # #     =========
        # #
        # #     List all users to manage them.
        # #
        # #     """
        # #
        # #     def get(self, request):
        # #
        # #         contact_list = User.objects.all()
        # #         paginator = Paginator(contact_list, USERS_PER_PAGE)
        # #
        # #         page = request.GET.get('page')
        # #         try:
        # #             users = paginator.page(page)
        # #         except PageNotAnInteger:
        # #             # If page is not an integer, deliver first page.
        # #             users = paginator.page(1)
        # #         except EmptyPage:
        # #             # If page is out of range (e.g. 9999), deliver last page of results.
        # #             users = paginator.page(paginator.num_pages)
        # #
        # #         return render_to_response('django_users_login/list.html', {"users": users})
        # #
        # #         # def dispatch(self, request, *args, **kwargs):
        # #         #     """
        # #         #     dispach
        # #         #     -------
        # #
        # #         #     If the current user is not admin then
        # #         #     """
        # #
        # #         #     if request.user.is_authenticated():
        # #         #         return super(ListUsers, self).dispatch(request, *args, **kwargs)